#include <stdio.h>
#include <stdlib.h>
#include "../header/arg.h"

int
main (int argc, char **argv){

    parsing(argc, argv);
    const char *name = getOption("-name");
    fprintf (stdout, "My Name %s.\n", name);

    outAllOption(stdout);

    return EXIT_SUCCESS;
}
