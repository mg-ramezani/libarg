CC=gcc
OP=-O3 -o
SH=-c -fPIC

ARGS=source/arg.c header/arg.h
ARGO=object/arg.o

binary/libarg.so : $(ARGO)
	$(CC) -shared $(OP) binary/libarg.so $(ARGO)

$(ARGO) : $(ARGS)
	$(CC) $(SH) $(OP) object/arg.o source/arg.c

clean : 
	yes | rm -fv ./binary/*
	yes | rm -fv ./object/*

buildTest : 
	$(CC) $(OP) binary/arg -larg -Lbinary/ -Wl,-rpath=binary/ test/main.c
