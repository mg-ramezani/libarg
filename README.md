# Argument Parser
a quite simple argument parser.
## Requirements
* GCC Compiler
* GNU Make
## Build
* using `make` :</br>
> `make`
* build test:</br>
> `make buildTest`</br>
> `binary/arg -name Ghasem -family Ramezani`</br>
> __see:__ `test/main.c`
## Usage
first include `arg.h` in `header` file. and pass `argc`, `argv` to `parsing()` function. like `parsing(argc, argv);` then you can take a option value.
```C
int
main (int argc, char **argv){
    parsing(argc, argv);
    const char *name = getOption("-name");
    .
    .
```
use `outAllOption(FILE)` to see all available command line arguments.
```C
int
main (int argc, char **argv){
    parsing(argc, argv);
    outAllOption(stderr);
    .
    .
```

