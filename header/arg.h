#ifndef HEADER_ARG_H
#define HEADER_ARG_H

#include <stdbool.h>
#include <stdio.h>

/*
 * We Use MAX_ARG To Define 
 * Maximum Argmunts.
 * Defualt is 10
 */
#define MAX_ARG 10

/*
 * Keep Tracking Of Available
 * Args.
 */
extern unsigned top;

/*
 * Structure ARGS Used For
 * Keaping Each Arguments
 * Whit Value.
 */
struct ARGS{
    const char *name;
    const char *value;
};
extern struct ARGS listArgs[MAX_ARG];

void parsing(int, char **);

char *getOption(const char *);

void outAllOption(FILE *);

#endif
