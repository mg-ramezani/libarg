#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../header/arg.h"

unsigned top=0;
struct ARGS listArgs[MAX_ARG];

void parsing(int argc, char **argv){
    top = 0;
    for (unsigned i=1; i < (unsigned)argc; i+=2){
        listArgs[top].name    = argv[i];
        listArgs[top].value   = argv[i+1];
        top++;
    }
}

char *getOption(const char *key){
    for (unsigned i=0; i < top; ++i){
        if (strcmp(listArgs[i].name, key) == 0){
            if (listArgs[i].value == NULL)
                return "NULL";
            else
                return listArgs[i].value;
        }
    }
    return "NULL";
}

void outAllOption(FILE *stream){
    for (unsigned i=0; i < top; ++i){
        fprintf(stream, "[%s]= [%s]\n",
                listArgs[i].name, listArgs[i].value);
    }
}
